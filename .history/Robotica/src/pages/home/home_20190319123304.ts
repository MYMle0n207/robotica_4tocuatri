import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { ObjetoItem } from '../../models/Objeto.interface';
import { AlertController } from 'ionic-angular';
import { Tab1 } from '../contact/contact';
import { Tab2 } from './tab2-page';



//ionic serve -l
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  ObjetosListRef$: FirebaseListObservable<ObjetoItem[]>
  ObjetosListRef2$: FirebaseListObservable<ObjetoItem[]>
  
  Animal = 'Perro';
  Animales = ['Perro',"Gato",'Chivo',"Borrego"];
  items = [{'item':'Perro'},{'item':'Taza'}];
  Estado:any;
  Foto:any;
  valor:JSON;
  tab1: any;
  tab2: any;
  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public storage: Storage,private database: AngularFireDatabase) {
    this.tab1 = Tab1;
    this.tab2 = Tab2;
    /*
    this.platform.ready().then((rdy) => {
      this.localNotifications.on('click', (notification, state) => {
        let json = JSON.parse(notification.data);
        
        let alert = this.alertCtrl.create({
          title: notification.title,
          subTitle: json.mydata
        });
        alert.present();
      });
    });
  */
    
    //Obtener valor de estado en el storage
    this.storage.get('Estado').then((datas)=>{
      this.Estado = datas;
    });  
    this.ObjetosListRef$ = this.database.list('Objetos');

    this.storage.get('Estado').then((dato)=>{
      this.Foto = dato;
    });  
    this.ObjetosListRef2$ = this.database.list('Sensores');
    
    
  }
  /*
  btnPushClicked(){
    this.platform.ready().then(() => {
      this.localNotifications.schedule({
        title: "Mi primera notificacion ",
        text: 'Tenemos notificaciones PRRO',
        trigger: {at: new Date(new Date().getTime() + 3600)},              
     });
    });
  }
/*
/*
  btnPushClicked(){
    this.localNotifications.schedule({
      id: 1,
      title: "Atencion",
      text: "Simona la mona",
      //at: new Date(new Date().getTime() + 5 *1000),
      data: {mydata: "My informacion oculta"}
    })
  }
  */
  EST(objeto,Estado){
    if(Estado===true){
      this.database.object('Objetos/'+objeto.$key).update({'Estado':false});
    }else{
      this.database.object('Objetos/'+objeto.$key).update({'Estado':true});
    }
   }

   
   UPD(objeto, Estado){
    //this.database.object('Sensores/'+objeto.$key).update({'Estado':1});
    let g = this.database.object('Sensores/'+objeto.$key)
    
    console.log(g)
    
   }
   

      
}
